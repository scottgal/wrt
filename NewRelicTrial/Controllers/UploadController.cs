﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using NewRelicTrial.Webp;

namespace NewRelicTrial.Controllers
{
    public class UploadController : ApiController
    {
        [HttpPost]
        public async Task<HttpResponseMessage>  UploadFile()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var root = HttpContext.Current.Server.MapPath("~/Images/Uploads/");
            var provider = new MultipartFormDataStreamProvider(root);
         
            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);
                Parallel.ForEach(provider.FileData, file =>
                {
                    var fileName = file.Headers.ContentDisposition.FileName;
                    var newFileName = string.Format("{0}.{1}", file.LocalFileName,
                        Path.GetExtension(file.Headers.ContentDisposition.FileName.Replace("\"", "")));
                    File.Move(file.LocalFileName, newFileName);

                    var webPFile = WebPUtil.ConvertFile(newFileName)
                        ;


                });
                return new HttpResponseMessage()
                    {
                        Content = new StringContent(""),
                        StatusCode = HttpStatusCode.OK
                    };
            }
            catch (Exception e)
            {
                {
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
                }
                throw;
            }
    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Error!");
        }
    }
}
