﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NewRelicTrial.Startup))]
namespace NewRelicTrial
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
