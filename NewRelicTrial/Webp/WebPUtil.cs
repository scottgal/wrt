﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;

namespace NewRelicTrial.Webp
{
    public class WebPUtil
    {



        public async static Task<string> ConvertFile(string fileName)
        {
            var source = new Bitmap(fileName);
            var outName = Path.GetFileNameWithoutExtension(fileName);
            var outDir = Path.GetDirectoryName(fileName);
            var data = source.LockBits(
                new Rectangle(0, 0, source.Width, source.Height),
                ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);
            IntPtr unmanagedData;
            var size = WebPEncodeBGR(data.Scan0, source.Width, source.Height, data.Stride, 75, out unmanagedData);
            var managedData = new byte[size];
            Marshal.Copy(unmanagedData, managedData, 0, size);
            var outFileName = Path.Combine(outDir, outName + ".webp");
            using (var fs = new FileStream(outFileName, FileMode.CreateNew))
            {
              fs.Write(managedData, 0, managedData.Length);
            }
         
            //File.WriteAllBytes(outFileName, managedData);


            WebPFree(unmanagedData);
            return outFileName;
        }

        [DllImport("libwebp.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int WebPEncodeBGR(IntPtr rgb, int width, int height, int stride, float quality_factor,
            out IntPtr output);
        [DllImport("libwebp.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int WebPEncodeBGRA(IntPtr rgb, int width, int height, int stride, float quality_factor,
            out IntPtr output);
        [DllImport("libwebp.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int WebPEncodeRGB(IntPtr rgb, int width, int height, int stride, float quality_factor,
            out IntPtr output);

        [DllImport("libwebp.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int WebPEncodeRGBA(IntPtr rgb, int width, int height, int stride, float quality_factor,
            out IntPtr output);



        [DllImport("libwebp.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int WebPFree(IntPtr p);

    }
}